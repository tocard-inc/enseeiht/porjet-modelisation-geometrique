import matplotlib.pyplot as plt
import numpy as np
from math import comb


def B(n, k, t):
    return comb(n, k) * t**k * (1 - t)**(n - k)


P = np.array([[0, 0], [0, 2], [1, 2], [1, 0], [1, -2], [2, -1], [2, 0]])

n = 3

S1 = np.array([sum(P[k] * B(n, k, t) for k in range(n + 1)) for t in np.linspace(0, 1, 100)])
S2 = np.array([sum(P[k + n] * B(n, k, t) for k in range(n + 1)) for t in np.linspace(0, 1, 100)])

S1p = np.array([n * sum((P[k + 1] - P[k]) * B(n - 1, k, t) for k in range(n)) for t in np.linspace(0, 1, 100)])
S2p = np.array([n * sum((P[k + 1 + n] - P[k + n]) * B(n - 1, k, t) for k in range(n)) for t in np.linspace(0, 1, 100)])

plt.subplot(121)
plt.plot(S1[:,0], S1[:,1])
plt.plot(S2[:,0], S2[:,1])
plt.plot(P[:,0], P[:,1], 'ro')
plt.axis('equal')

plt.subplot(122)
plt.plot(S1p[:,0], S1p[:,1])
plt.plot(S2p[:,0], S2p[:,1])
plt.axis('equal')
plt.show()


