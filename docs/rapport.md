<h1 align="center">
Projet de Modélisation Géométrique<br>
Création et suivi de trajectoire de caméras<br>
Option longue 1
</h1>

<style>
body {
    text-align: justify;
    max-width: 60rem;
}
</style>

<p align="center">
<a href="mailto:laurent.fainsin@etu.inp-n7.fr">Laurent Fainsin</a> &mdash;
<a href="mailto:damien.guillotin@etu.inp-n7.fr">Damien Guillotin</a>
</p>

<p align="center">
<a href="https://git.inpt.fr/tocard-inc/enseeiht/porjet-modelisation-geometrique">Lien vers le répertoire</a>
</p>

## Description

L’objectif de ce projet était de construire, à partir des connaissances acquises durant le cours d’interpolation et d’approximation, un module sous Unity3D qui permet de réaliser un suivi de trajectoire de caméra en ne fournissant qu’un nombre restreint d’informations, à savoir quelques points (trois coordonnées spatiales, trois coordonnées axiales).

Nous avons décidé (avec l'accord de notre professeur, Julien Desvergnes) de modifier légèrement la consigne de ce projet. Nous avons opté pour le developpement d'un plugin pour un serveur [Minecraft](https://www.minecraft.net/fr-fr) [Spigot](https://www.spigotmc.org/), permettant aux joueurs de créer des travelings.

## API Spigot

L'API Spigot nous permet d'interfacer avec le monde / les propriétés / les entités de notre serveur Minecraft.

Le serveur Minecraft ([paperMC](https://papermc.io/)) étant écrit en Java, nous devons utiliser ce langage de programmation pour développer notre plugin.

Pour développer notre plugin nous avons donc créé un environnement de developpement sous VSCode via le gestionnaire de dépendance Java: [Gradle](https://gradle.org/).

Pour compiler notre plugin nous pouvons générer une archive `.jar` via la commande `gradle jar`. Pour déployer notre plugin sur notre serveur, nous plaçons notre archive dans le dossier `plugins` de notre serveur.

Pour améliorer le déployment et le lancement du serveur Minecraft, nous avons de plus utilisé un [docker-compose](https://docs.docker.com/compose/).

Une fois le plugin déployé, et le serveur démarré, nous pouvons utiliser notre plugin. Si celui-ci n'est pas activé, nous pouvons utiliser la commande `/reload confirm` pour relancer les plugins.

## Choix de courbe

Pour le traveling de notre caméra, nous utilisons des courbes de Bezier. Pour réaliser la courbe complète, nous faisons suivre des courbes de degrés 3.

La courbe finale s'apparente à une spline mais nous avons choisi ce type de modélisation car nous souhaitons que notre caméra passe exactement par certains points de contrôles (interpolation). Il est ainsi plus intuitif pour l'utilisateur de modifier la courbe.

Ces courbes de bézier sont générées par évaluation, nous avons fait ce choix car la subdivision semblait peu adapté à la génération d'un nombre de point précis. De plus, le temps de calcule de la courbe reste négligeable, l'optimisation temporelle que permettrait la subdivision ne serait pas perceptible.

Puisque la caméra dans Minecraft ne permet pas de rotation "row", nous avons directement interpolé nos rotations selon un schéma de bézier (sans passer par des quaternions).

### Avantages / Inconvénients

Grace a cette méthode, nous pouvons interpolé des points tout en gardant le contrôle sur la courbe. On arrive donc à obtenir une courbe très maléable tout en restant stable. Parcontre, le placement des points de contrôle peut devenir assez long si l'on souhaite obtenir une trajectoire très precise étant donné qu'il y a quatre points à gérer par segment.

Un autre point positif de cette construction est que nous avons le choix de positionner les points (grâce a une commande) de sorte que la courbe soit $C^1$.

### Démonstrations mathématiques

Les polynomes de Bernstein sont definit comme étant :

$\displaystyle B_k^n(t) = \binom{n}{k} t^k (1 - t)^{n-k}$

Le $i^{ème}$ tronçon de courbe est alors définit par :

$\displaystyle S_i(t) = \sum_{k=0}^{n} P_i^k\ B_k^n(t)$

Pour garder le caractère $C^1$ de la courbe, il faut donc que la dérivé en $1$ du $i^{ème}$ tronçon soit égale à la dérivé en $0$ du $i^{ème}+1$.

Soit,
$\displaystyle S_i'(1) = S_{i + 1}'(0)$

avec,
$\displaystyle S_i'(t) = n \sum_{k=0}^{n - 1} (P_i^{k + 1} - P_i^k)\ B_k^{n - 1}(t)$

Ce qui fait :

$\displaystyle n (P_i^{n} - P_i^{n - 1}) = n (P_{i + 1}^{1} - P_{i + 1}^{0})$

Le résultat $\displaystyle P_i^{n} - P_i^{n - 1} = P_{i + 1}^{1} - P_{i + 1}^{0}$ se traduit géométriquement par l'alignement et l'équidistance des points de contrôle à l'ancre à la qu'elle ils sont ratachés.

## Démonstrations

<style>
video {
    max-width: 100%;
}
</style>

<table>
<tr>
    <td>
        <video src="https://fainsil.users.inpt.fr/content/ModéGéom/tuto.webm" autoplay loop controls></video>
    </td>
    <td>
        <video src="https://fainsil.users.inpt.fr/content/ModéGéom/circle.webm" autoplay loop controls></video>
    </td>
</tr>
<tr>
    <td>
        <video src="https://fainsil.users.inpt.fr/content/ModéGéom/waterfall.webm" autoplay loop controls></video>
    </td>
    <td>
        <video src="https://fainsil.users.inpt.fr/content/ModéGéom/island.webm" autoplay loop controls></video>
    </td>
</tr>
<tr>
    <td>
        <video src="https://fainsil.users.inpt.fr/content/ModéGéom/demo_show.webm" autoplay loop controls></video>
    </td>
    <td>
        <video src="https://fainsil.users.inpt.fr/content/ModéGéom/demo_noshow.webm" autoplay loop controls></video>
    </td>
</tr>
<tr>
    <td>
        <video src="https://fainsil.users.inpt.fr/content/ModéGéom/full_show.webm" autoplay loop controls></video>
    </td>
    <td>
        <video src="https://fainsil.users.inpt.fr/content/ModéGéom/full_noshow.webm" autoplay loop controls></video>
    </td>
</tr>
</table>

## Conclusion

Notre interpolation fonctionne bien, nos résultats sont satisfaisants.

Seul bémol, notre serveur fonctionne assez lentement par rapport au serveur.
Tandis qu'un client Minecraft tourne au minimum à 60 fps, notre serveur étant monothreadé, celui-ci "tourne" plutôt aux alentour des 20 tps. On obtient alors un rendu avec quelques secousses dans certains cas.

## Les commandes utiles

`/show` permet d'afficher/cacher la courbe et les points de contrôle.

`/exec [true/false] [#start] [#end]` permet de lancer le traveling en commençant à la `#start` courbe et en s'arretant à la `#end`. L'attribut `true/false` permet de se déplacer de manière plus fluide le long de la courbe (cependant il est expérimental, il n'y a pas d'interpolation des rotations).

`/close` : ferme/ouvre la courbe et passe en mode repeat/simple.

`/point <add|rm|set|fix> [index]` \
`add` : ajoute un point à la suite de la courbe. \
`rm` : enlève le groupe de point indiqué. \
`set` : déplace le point indiqué. \
`fix` : modifie le point indiqué de sorte à ce que la courbe soit $C^1$.

`/reset` : supprime l'entièreté des points.

`/save <file>` : permet de sauvegarder les points actuels dans un fichier.

`/load <file>` : permet de charger les points dans un fichier.

`/points` : permet de lister l'ensemble de points de contrôle éxistant.

`/runas <player> <command>` : exécute une commande à la place d'un autre joueur.
