package com.tocard.cam;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.EulerAngle;
import org.bukkit.Bukkit;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.bukkit.Particle;
import org.bukkit.World;
import org.bukkit.Material;

public class ShowCurve implements CommandExecutor {

    public static List<ArmorStand> controlPointsArmorStands = new ArrayList<>();
    private static ItemStack cameraHeadAnchor = new ItemStack(Material.OBSERVER);
    private static ItemStack cameraHeadControl = new ItemStack(Material.DISPENSER);
    private static int taskID = -1;

    // This method is called, when somebody uses our command
    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (sender instanceof Player) {
            Player player = (Player) sender;

            if (taskID >= 0) {
                // Cancel show task
                Bukkit.getScheduler().cancelTask(taskID);
                taskID = -1;

                // Kill control points' armorstand
                for (ArmorStand as : controlPointsArmorStands) {
                    as.getEquipment().setHelmet(new ItemStack(Material.AIR));
                    as.setCustomNameVisible(false);
                }
                return true;
            }

            // Show control points
            int i = 0;
            for (ArmorStand as : controlPointsArmorStands) {
                as.setCustomNameVisible(true);
                if (i % 3 == 0)
                    as.getEquipment().setHelmet(cameraHeadAnchor);
                else
                    as.getEquipment().setHelmet(cameraHeadControl);
                i++;
            }

            // Start showing curve
            taskID = Bukkit.getScheduler().scheduleSyncRepeatingTask(Camera.plugin,
                    new Runnable() {
                        @Override
                        public void run() {
                            // Get curve iterator
                            Iterator<ExtendedLocation> curveIterator = Camera.curve.iterator();

                            // Draw path
                            while (curveIterator.hasNext()) {
                                player.getWorld().spawnParticle(Particle.ELECTRIC_SPARK,
                                        curveIterator.next().clone()
                                                .add(0, 1.8, 0),
                                        1,
                                        0, 0, 0, 0);
                            }

                            // Draw control lines
                            if (Camera.controlPoints.size() >= 4) {
                                int nAnchor = (Camera.controlPoints.size() - 1) / 3 + 1;
                                for (int i = 0; i < nAnchor; i++) {
                                    int indMin = Math.max(3 * i - 1, 0);
                                    int indMax = Math.min(3 * i + 1, Camera.controlPoints.size() - 1);

                                    for (float t = 0; t < 1; t += 0.05) {
                                        player.getWorld().spawnParticle(Particle.COMPOSTER,
                                                Camera.controlPoints.get(indMin).clone().multiply(1 - t)
                                                        .add(Camera.controlPoints.get(indMax).clone()
                                                                .multiply(t))
                                                        .add(0, 1.8, 0),
                                                1,
                                                0, 0, 0, 0);
                                    }
                                }
                            }
                        }
                    }, 0, 0);

            // Broadcast TaskID
            Bukkit.broadcastMessage("Show curve : " + taskID);

        }

        return true;
    }

    public static void add(ExtendedLocation point, World world) {
        ArmorStand as = world.spawn(point, ArmorStand.class);
        as.setGravity(false);
        as.setVisible(false);
        as.setMarker(true);
        as.addScoreboardTag("controlPoint");
        as.setCustomName("N°" + (controlPointsArmorStands.size()));
        if (taskID > 0) {
            as.setCustomNameVisible(true);
            if ((Camera.controlPoints.size() - 1) % 3 == 0)
                as.getEquipment().setHelmet(cameraHeadAnchor);
            else
                as.getEquipment().setHelmet(cameraHeadControl);
        }
        as.setHeadPose(new EulerAngle(point.getPitch() / 180 * Math.PI, 0, 0));
        controlPointsArmorStands.add(as);
    }

    @Deprecated
    public static void insert(int index, ExtendedLocation point, World world) {
        ArmorStand as = world.spawn(point, ArmorStand.class);
        as.setGravity(false);
        as.setVisible(false);
        as.setMarker(true);
        as.addScoreboardTag("controlPoint");
        if (taskID > 0)
            if ((Camera.controlPoints.size()) % 3 == 0)
                as.getEquipment().setHelmet(cameraHeadAnchor);
            else
                as.getEquipment().setHelmet(cameraHeadControl);
        as.setHeadPose(new EulerAngle(point.getPitch() / 180 * Math.PI, 0, 0));
        controlPointsArmorStands.add(index, as);
    }

    public static void set(int index, ExtendedLocation point, World world) {
        controlPointsArmorStands.get(index).setHeadPose(new EulerAngle(point.getPitch() / 180 * Math.PI, 0, 0));
        controlPointsArmorStands.get(index).teleport(point);
    }

    public static void rm(int index) {
        controlPointsArmorStands.remove(index).remove();

        // Show IDs
        int i = 0;
        for (ArmorStand as : controlPointsArmorStands) {
            as.setCustomName("N°" + i);
            if (taskID > 0)
                as.setCustomNameVisible(true);
            i++;
        }
    }
}
