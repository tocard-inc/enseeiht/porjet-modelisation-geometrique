package com.tocard.cam;

import org.bukkit.Location;

public class ExtendedLocation extends Location {

    public ExtendedLocation(Location location) {
        super(location.getWorld(),
                location.getX(),
                location.getY(),
                location.getZ(),
                location.getYaw(),
                location.getPitch());
    }

    public ExtendedLocation patchYaw(ExtendedLocation previousLocation) {
        float A = previousLocation.getYaw();
        float B = getYaw();
        float b = B % 360;
        int wholePart = 360 * (int) (A / 360);

        float minDist = -1;
        float sol = 0;
        for (int i = -1; i < 2; i++) {
            float val = wholePart + b + i * 360;
            float dist = Math.abs(val - A);
            if (minDist == -1 || dist < minDist) {
                sol = val;
                minDist = dist;
            }
        }

        setYaw(sol);
        return this;
    }

    public ExtendedLocation add(ExtendedLocation loc) {
        super.add(loc);

        setYaw(getYaw() + loc.getYaw());
        setPitch(getPitch() + loc.getPitch());

        return this;
    }

    public ExtendedLocation subtract(ExtendedLocation loc) {
        super.subtract(loc);

        setYaw(getYaw() - loc.getYaw());
        setPitch(getPitch() - loc.getPitch());

        return this;
    }

    public ExtendedLocation multiply(double m) {
        super.multiply(m);

        setYaw((float) m * getYaw());
        setPitch((float) m * getPitch());

        return this;
    }

    @Override
    public ExtendedLocation clone() {
        ExtendedLocation cloned = new ExtendedLocation(this);
        return cloned;
    }
}
