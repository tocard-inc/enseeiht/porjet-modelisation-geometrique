package com.tocard.cam;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class RunAs implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (sender instanceof Player) {
            Player playerRunner = Bukkit.getPlayer(args[0]);

            String cmd = "";
            for (int i = 1; i < args.length; i++) {
                cmd += args[i] + " ";
            }

            playerRunner.chat(cmd);
        }
        return true;
    }

}
