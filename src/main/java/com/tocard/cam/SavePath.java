package com.tocard.cam;

import java.io.IOException;
import java.io.PrintWriter;

import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class SavePath implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (sender instanceof Player) {
            save(args[0]);
        }
        return true;
    }

    public static void save(String file) {
        try {
            PrintWriter writer = new PrintWriter("plugins/tocard-cam/paths/" + file, "UTF-8");
            for (Location loc : Camera.controlPoints) {
                writer.println(
                        loc.getX() + "," + loc.getY() + "," + loc.getZ()
                                + "," + loc.getYaw() + "," + loc.getPitch());
            }
            writer.close();
        } catch (IOException e) {
            // do something
        }
    }

}
