package com.tocard.cam;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class NewPoint implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        try {
            if (sender instanceof Player) {
                Player player = (Player) sender;
                switch (args[0]) {
                    case "add":
                        add(args, new ExtendedLocation(player.getLocation()), player.getWorld());
                        break;
                    case "rm":
                        rm(args, null, null);
                        break;
                    case "set":
                        set(args, new ExtendedLocation(player.getLocation()), player.getWorld());
                        break;
                    case "fix":
                        fix(args, null, null);
                        break;
                    case "ins":
                        ins(args, new ExtendedLocation(player.getLocation()), player.getWorld());
                        break;
                    default:
                        break;
                }
            }
        } catch (Exception e) {
            Camera.broadlog("Wrong command usage");
            e.printStackTrace();
        }

        // Update camera path
        Camera.compute();
        return true;
    }

    public static void addPoint(ExtendedLocation point, World world) {
        Camera.controlPoints.add(point);
        ShowCurve.add(point, world);
        Camera.broadlog("Point added: " + Camera.prettyLocation(point));
    }

    public static void setPoint(int index, ExtendedLocation point, World world) {
        Camera.controlPoints.set(index, point);
        ShowCurve.set(index, point, world);
        Camera.broadlog("Point n°" + index + " set: " + Camera.prettyLocation(point));
    }

    public static void rmPoint(int index) {
        ExtendedLocation location = Camera.controlPoints.remove(index);
        ShowCurve.rm(index);
        Camera.broadlog("Point deleted: " + Camera.prettyLocation(location));
    }

    public static void add(String[] args, ExtendedLocation location, World world) {
        int n = Camera.controlPoints.size();

        if (n < 4) {
            addPoint(location, world);
        } else {
            ExtendedLocation P2 = Camera.controlPoints.get(n - 2);
            ExtendedLocation P3 = Camera.controlPoints.get(n - 1);
            location.patchYaw(P3);
            ExtendedLocation P4 = P3.clone().multiply(2).subtract(P2);
            ExtendedLocation P5 = location.clone().add(P4).multiply(0.5);

            addPoint(P4, world);
            addPoint(P5, world);
            addPoint(location, world);
        }
    }

    @Deprecated
    public static void ins(String[] args, ExtendedLocation location, World world) {
        int index = Integer.parseInt(args[1]);
        Camera.controlPoints.add(index, location);
        Camera.broadlog("Point added: " + Camera.prettyLocation(location));
        ShowCurve.insert(index, location, world);
    }

    public static void rm(String[] args, Location location, World world) {
        int index = Integer.parseInt(args[1]);
        int n = Camera.controlPoints.size();

        if (n <= 4) {
            rmPoint(index);
            return;
        }

        int indMin;
        if (index == 0 || index == 1) {
            indMin = 0;
        } else if (index == n - 1 || index == n - 2) {
            indMin = n - 3;
        } else {
            indMin = (index - 2) / 3 * 3 + 2;
        }

        for (int i = 0; i < 3; i++) {
            rmPoint(indMin);
        }
        return;

    }

    public static void set(String[] args, ExtendedLocation location, World world) {
        int n = Camera.controlPoints.size();
        int index = Integer.parseInt(args[1]);

        if (index > 0)
            location.patchYaw(Camera.controlPoints.get(index - 1));

        if (n <= 4) {
            setPoint(index, location, world);
            return;
        }

        switch (index % 3) {
            case 0:
                // Anchor point
                ExtendedLocation shift = location.clone().subtract(Camera.controlPoints.get(index));
                for (int i = 0; i < 3; i++) {
                    try {
                        setPoint(index + i - 1, Camera.controlPoints.get(index + i - 1).clone().add(shift), world);
                    } catch (IndexOutOfBoundsException e) {
                        // First or last anchor
                    }
                }
                break;
            case 1:
                // Control point after an anchor
                if (index - 2 >= 0) {
                    // Get anchor-location direction
                    ExtendedLocation anchor = Camera.controlPoints.get(index - 1);
                    double currentDistance = anchor.distance(location);
                    ExtendedLocation currentVect = anchor.clone().subtract(location).multiply(1 / currentDistance);

                    // Get anchor-corresponding distance
                    ExtendedLocation correspondingControl = Camera.controlPoints.get(index - 2);
                    double correspondingDistance = anchor.distance(correspondingControl);

                    // Keep correcponding at the same distance
                    // but along the anchor-location direction
                    setPoint(index - 2, anchor.clone().add(currentVect.multiply(correspondingDistance)), world);
                }
                // Move control point
                setPoint(index, location, world);
                break;
            case 2:
                // Control point before an anchor
                if (index + 2 < n) {
                    // Get anchor-location direction
                    ExtendedLocation anchor = Camera.controlPoints.get(index + 1);
                    double currentDistance = anchor.distance(location);
                    ExtendedLocation currentVect = anchor.clone().subtract(location).multiply(1 / currentDistance);

                    // Get anchor-corresponding distance
                    ExtendedLocation correspondingControl = Camera.controlPoints.get(index + 2);
                    double correspondingDistance = anchor.distance(correspondingControl);

                    // Keep correcponding at the same distance
                    // but along the anchor-location direction
                    setPoint(index + 2, anchor.clone().add(currentVect.multiply(correspondingDistance)), world);
                }
                // Move control point
                setPoint(index, location, world);
                break;

        }

    }

    public static void fix(String[] args, ExtendedLocation location, World world) {
        int n = Camera.controlPoints.size();
        int index = Integer.parseInt(args[1]);

        if (n <= 4 || index < 2 || index > n - 3) {
            return;
        }

        ExtendedLocation P0, P1, P2;
        switch (index % 3) {
            case 0:
                // Anchor point
                P0 = Camera.controlPoints.get(index - 1);
                P2 = Camera.controlPoints.get(index + 1);
                P1 = P0.clone().add(P2).multiply(0.5);

                setPoint(index, P1, P1.getWorld());
                break;
            case 1:
                // Control point after an anchor
                P0 = Camera.controlPoints.get(index - 2);
                P1 = Camera.controlPoints.get(index - 1);
                P2 = P1.clone().multiply(2).subtract(P0);

                setPoint(index, P2, P2.getWorld());
                break;
            case 2:
                // Control point before an anchor
                P1 = Camera.controlPoints.get(index + 1);
                P2 = Camera.controlPoints.get(index + 2);
                P0 = P1.clone().multiply(2).subtract(P2);

                setPoint(index, P0, P0.getWorld());
                break;

        }

    }
}
