package com.tocard.cam;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class ClosePath implements CommandExecutor {

    public static boolean closed = false;

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (sender instanceof Player) {
            close();
        }

        return true;
    }

    public void close() {
        if (!closed && Camera.controlPoints.size() >= 4) {
            NewPoint.add(null, Camera.controlPoints.get(0), Camera.controlPoints.get(0).getWorld());

            NewPoint.set(new String[] { "", "" + (Camera.controlPoints.size() - 2) },
                    Camera.controlPoints.get(0).clone().multiply(2).subtract(Camera.controlPoints.get(1)),
                    Camera.controlPoints.get(0).getWorld());

            SavePath.save("tmp");
            LoadPath.load("tmp");

            closed = true;
        } else if (closed) {
            closed = false;

            NewPoint.rm(new String[] { "", "" + (Camera.controlPoints.size() - 1) }, null, null);
        }

        Camera.compute();
    }

}
