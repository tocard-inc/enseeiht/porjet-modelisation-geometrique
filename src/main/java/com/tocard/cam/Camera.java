package com.tocard.cam;

import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.inventory.ItemStack;

public class Camera extends JavaPlugin implements Listener {

    public static ArrayList<ExtendedLocation> curve = new ArrayList<>();
    public static ArrayList<ExtendedLocation> controlPoints = new ArrayList<>();
    public static Logger logger;
    public static Plugin plugin;
    public static int nbSubdiv = 100;

    @Override
    public void onEnable() {
        plugin = this;

        // init logger
        logger = getLogger();

        // init locations List
        controlPoints = new ArrayList<>();

        // setup commands
        this.getCommand("point").setExecutor(new NewPoint());
        this.getCommand("points").setExecutor(new ListPoints());
        this.getCommand("reset").setExecutor(new ClearPoints());
        this.getCommand("exec").setExecutor(new ExecuteTraveling());
        this.getCommand("show").setExecutor(new ShowCurve());
        this.getCommand("close").setExecutor(new ClosePath());
        this.getCommand("load").setExecutor(new LoadPath());
        this.getCommand("save").setExecutor(new SavePath());
        this.getCommand("move").setExecutor(new Move());
        this.getCommand("runas").setExecutor(new RunAs());

        // Eventhandlers
        getServer().getPluginManager().registerEvents(this, this);
    }

    @Override
    public void onDisable() {
        ClearPoints.clear();
    }

    // ----------------------------------------------------------------------------------------------------

    @EventHandler
    public void onInteract(PlayerInteractEvent event) {
        Action action = event.getAction();
        Player player = event.getPlayer();

        if (action.equals(Action.RIGHT_CLICK_AIR) || action.equals(Action.RIGHT_CLICK_BLOCK)) {
            ItemStack helditem = player.getPlayer().getInventory().getItemInMainHand();
            switch (helditem.getType()) {
                case STICK:
                    player.chat("/point add");
                    break;
                case PAPER:
                    player.chat("/show");
                    break;
                case SLIME_BALL:
                    player.chat("/exec");
                    break;
                case BRICK:
                    ClearPoints.clear();
                    break;
                case BONE:
                    player.chat("/reset 0");
                    break;
                default:
                    break;
            }
        }
    }

    public static void patchYaws() {
        for (int i = 1; i < controlPoints.size(); i++) {
            controlPoints.get(i).patchYaw(controlPoints.get(i - 1));
        }
    }

    public static void compute() {
        patchYaws();

        float dt = 1 / (float) nbSubdiv;

        int nControlPoints = controlPoints.size();
        curve = new ArrayList<>();

        if (nControlPoints == 0) {
            return;
        }

        if (nControlPoints <= 4) {
            for (float t = 0; t < 1; t += dt) {
                ArrayList<ExtendedLocation> P = new ArrayList<>(controlPoints);

                int N = P.size();
                for (int k = N - 1; k > 0; k--) {
                    for (int i = 0; i < k; i++) {
                        P.set(i, P.get(i).clone().multiply(1 - t)
                                .add(P.get(i + 1).clone().multiply(t)));
                    }
                }

                curve.add(P.get(0));
            }
            return;
        }

        int nBezier = (nControlPoints - 1) / 3;
        for (int iBezier = 0; iBezier < nBezier; iBezier++) {
            for (float t = 0; t < 1; t += dt) {
                ArrayList<ExtendedLocation> P = new ArrayList<>();
                for (int k = 0; k < 4; k++) {
                    P.add(controlPoints.get(3 * iBezier + k).clone());
                }

                int N = P.size();
                for (int k = N - 1; k > 0; k--) {
                    for (int i = 0; i < k; i++) {
                        P.set(i, P.get(i).clone().multiply(1 - t)
                                .add(P.get(i + 1).clone().multiply(t)));
                    }
                }
                curve.add(P.get(0));
            }
        }
    }

    public static String prettyLocation(Location point) {
        return String.format("X=%05.2f, Y=%05.2f, Z=%05.2f, P=%05.2f, Y=%05.2f",
                point.getX(), point.getY(), point.getZ(),
                point.getPitch(), point.getYaw());
    }

    public static void broadlog(String msg) {
        Camera.logger.log(Level.INFO, msg);
        Bukkit.broadcastMessage(msg);
    }

}
