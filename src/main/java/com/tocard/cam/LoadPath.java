package com.tocard.cam;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class LoadPath implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (sender instanceof Player) {
            load(args[0]);
        }
        return true;
    }

    public static void load(String file) {
        ClearPoints.clear();
        World world = Bukkit.getWorlds().get(0);

        BufferedReader reader;
        try {
            reader = new BufferedReader(new FileReader("plugins/tocard-cam/paths/" + file));
            String line = reader.readLine();
            while (line != null) {
                String[] coords = line.split(",");
                ExtendedLocation location = new ExtendedLocation(
                        new Location(world,
                                Float.parseFloat(coords[0]), Float.parseFloat(coords[1]), Float.parseFloat(coords[2]),
                                Float.parseFloat(coords[3]), Float.parseFloat(coords[4])));
                NewPoint.addPoint(location, world);

                line = reader.readLine();
            }
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        Camera.compute();
    }

}
