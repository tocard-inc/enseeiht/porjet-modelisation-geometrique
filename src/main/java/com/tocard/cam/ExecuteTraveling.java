package com.tocard.cam;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;

import java.util.Iterator;

public class ExecuteTraveling implements CommandExecutor {

    private static int taskID = -1;
    private static Iterator<ExtendedLocation> curveIterator;
    private static boolean useVelocity = false;

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (sender instanceof Player) {
            Player player = (Player) sender;

            int fromIndex = 0;
            int toIndex = (Camera.controlPoints.size() - 1) / 3;
            useVelocity = false;
            if (args.length > 0) {
                // Use moveTo intead of teleport
                useVelocity = Boolean.parseBoolean(args[0]);

                // Get starting and ending points
                int ind = useVelocity ? 1 : 0;
                try {
                    fromIndex = Integer.parseInt(args[ind]);
                    toIndex = Integer.parseInt(args[ind + 1]);
                } catch (Exception e) {
                }
            }

            if (taskID >= 0) {
                // Cancel show task
                Bukkit.getScheduler().cancelTask(taskID);
                taskID = -1;

                return true;
            }

            player.setGameMode(GameMode.SPECTATOR);

            curveIterator = Camera.curve.subList(fromIndex * (Camera.nbSubdiv + 1), toIndex * (Camera.nbSubdiv + 1))
                    .iterator();

            player.teleport(curveIterator.next());

            taskID = Bukkit.getScheduler().scheduleSyncRepeatingTask(Camera.plugin, new Runnable() {
                @Override
                public void run() {
                    // Teleport to next point
                    if (curveIterator.hasNext()) {
                        if (useVelocity)
                            Move.moveTo(player, curveIterator.next());
                        else
                            player.teleport(curveIterator.next());
                        return;
                    }

                    // Regenerate traveling and begin
                    if (ClosePath.closed) {
                        curveIterator = Camera.curve.iterator();
                        player.teleport(curveIterator.next());
                        return;
                    }

                    // Teleport to next point
                    int taskToKill = taskID;
                    taskID = -1;
                    Bukkit.getScheduler().cancelTask(taskToKill);
                }
            }, 0, 0);
            Bukkit.broadcastMessage("Traveling : " + taskID);
        }

        return true;
    }
}
