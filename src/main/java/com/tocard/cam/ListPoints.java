package com.tocard.cam;

import java.util.ListIterator;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class ListPoints implements CommandExecutor {

    private static ListIterator<ExtendedLocation> points;

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (sender instanceof Player) {
            Camera.broadlog("List of points:");
            listPoints();
        }
        return true;
    }

    protected static void listPoints() {
        // get the controlPoints iterator
        points = Camera.controlPoints.listIterator();

        // print them one by one
        while (points.hasNext()) {
            Camera.broadlog("Point n°" + points.nextIndex() + ": " + Camera.prettyLocation(points.next()));
        }
    }
}
