package com.tocard.cam;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

public class Move implements CommandExecutor {

    public static int taskID = -1;

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (sender instanceof Player) {
            if (sender instanceof Player) {
                Player player = (Player) sender;

                if (taskID >= 0) {
                    // Cancel show task
                    Bukkit.getScheduler().cancelTask(taskID);
                    taskID = -1;
                    return true;
                }

                double x = Float.parseFloat(args[0]);
                double y = Float.parseFloat(args[1]);
                double z = Float.parseFloat(args[2]);

                // Start showing curve
                taskID = Bukkit.getScheduler().scheduleSyncRepeatingTask(Camera.plugin,
                        new Runnable() {
                            @Override
                            public void run() {
                                moveTo(player, x, y, z);
                            }
                        }, 0, 0);

                // Broadcast TaskID
                Bukkit.broadcastMessage("Show curve : " + taskID);

            }

            return true;
        }
        return true;
    }

    public static void moveTo(Player player, double x, double y, double z) {
        Location delta = player.getLocation();
        double dx = x - delta.getX();
        double dy = y - delta.getY();
        double dz = z - delta.getZ();

        player.setVelocity(new Vector(dx, dy, dz).multiply(0.1));
    }

    public static void moveTo(Player player, Location location) {
        // rotateTo(player, location);
        Location delta = location.clone().subtract(player.getLocation());
        player.setVelocity(delta.toVector().multiply(0.1));
    }

    public static void rotateTo(Player player, Location location) {
        Location playerLocation = player.getLocation();
        playerLocation.setYaw(location.getYaw());
        playerLocation.setPitch(location.getPitch());
        player.teleport(playerLocation);
        Camera.broadlog(Camera.prettyLocation(playerLocation));
    }

}
