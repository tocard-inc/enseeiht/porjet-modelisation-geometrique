package com.tocard.cam;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.util.BlockIterator;
import org.bukkit.util.Vector;

public class ClearPoints implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (sender instanceof Player) {
            Player player = (Player) sender;
            if (args.length == 0) {
                clear();
            } else {
                Vector dir = player.getLocation().getDirection();
                player.teleport(getTargetBlock(player, 100).getLocation().setDirection(dir).add(0, 1, 0));
            }
        }
        return true;
    }

    public static void clear() {
        Camera.broadlog("Clearing points and path:");

        ListPoints.listPoints();

        while (Camera.controlPoints.size() > 0) {
            NewPoint.rm(new String[] { "", "0" }, null, null);
        }

        ClosePath.closed = false;

        Camera.compute();

        Camera.broadlog("All cleared !");
    }

    public static Block getTargetBlock(Player player, int range) {
        BlockIterator iter = new BlockIterator(player, range);
        Block lastBlock = iter.next();
        while (iter.hasNext()) {
            lastBlock = iter.next();
            if (lastBlock.getType() == Material.AIR) {
                continue;
            }
            break;
        }
        return lastBlock;
    }

}
